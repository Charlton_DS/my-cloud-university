﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCUCommon
{

    public class EnrollRequest
    {
        public EnrollRequest(String StudentId, String CourseId)
        {
            this.StudentId = StudentId;
            this.CourseId = CourseId;
        }

        public EnrollRequest(){}

        public String StudentId { get; set; }

        public String CourseId { get; set; }

        public String UniversityId { get; set; }

        public String Assignments { get; set; }

        public String CourseWork { get; set; }

        public String Grade { get; set; }

        public String Semester { get; set; }
        public override string ToString()
        {
            return String.Format("[StudentId: {0}, CourseId: {1}, Assignments: {2}, CourseWork: {3}]", StudentId, CourseId, Assignments, CourseWork);
        }
        
    }
}

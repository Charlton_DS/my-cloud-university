using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Queue;
using MCUCommon;
using System.Data.Entity;

namespace MeasurementWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);
        
        private CloudTable registrationTable;
        private CloudTable measurementTable;
        private CloudQueue measurementQueue;
        private MCUContext db;

        public override void Run()
        {
            Trace.TraceInformation("MeasurementWorker is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            var dbConnString = CloudConfigurationManager.GetSetting("MCUDbConnectionString");
            db = new MCUContext(dbConnString);

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            registrationTable = tableClient.GetTableReference("enrollment");
            registrationTable.CreateIfNotExists();
            measurementTable = tableClient.GetTableReference("measurement");
            measurementTable.CreateIfNotExists();

            CloudQueueClient queueCluient = storageAccount.CreateCloudQueueClient();
            measurementQueue = queueCluient.GetQueueReference("measurment");
            measurementQueue.CreateIfNotExists();

            bool result = base.OnStart();

            Trace.TraceInformation("MeasurementWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("MeasurementWorker is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("MeasurementWorker has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            CloudQueueMessage queueMessage = null;
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                
                try
                {
                    queueMessage = measurementQueue.GetMessage();
                    if (queueMessage != null)
                    {

                        String[] message = queueMessage.AsString.Split(',');
                        String Id = message[1];
                        Trace.TraceInformation("queue");
                        if (message[0] == "login")
                        {

                            University client = db.Universities
                                .Where(u => u.TableName.Equals(Id))
                                .FirstOrDefault();
                            if (client != null)
                            {
                                AddToLoginsForToday(client.TableName);

                                if (client.CurrentUsers + 1 <= client.ServicableUsers)
                                {
                                    client.CurrentUsers++;
                                    client.CurrentUsageCost = client.CurrentUsageCost + 1;
                                }
                                else if (client.AutoScale)
                                {
                                    client.CurrentUsers++;
                                    client.CurrentUsageCost = client.CurrentUsageCost + 1.5;
                                }
                                else
                                {
                                    long penalty = client.CurrentUsers / client.ServicableUsers;
                                    client.CurrentUsers++;
                                    client.CurrentUsageCost = client.CurrentUsageCost + penalty + 1;
                                }
                                db.Entry(client).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else if (message[0] == "logout")
                        {
                            University client = db.Universities
                            .Where(u => u.TableName.Equals(Id))
                            .FirstOrDefault();

                            if (client == null)
                                return;

                            client.CurrentUsers--;
                            db.Entry(client).State = EntityState.Modified;
                            db.SaveChanges();

                        }
                        measurementQueue.DeleteMessage(queueMessage);
                    }
                    
                }
                catch (Exception e)
                {
                    Trace.TraceInformation(e.ToString());
                    if (queueMessage != null && queueMessage.DequeueCount > 5)
                    {
                        measurementQueue.DeleteMessage(queueMessage);
                        Trace.TraceError("Deleting poison queue item: '{0}'", queueMessage.AsString);

                    }
                    
                }
                await Task.Delay(1000);
            }
        }

        private void AddToLoginsForToday(String ClientId)
        {
            DateTime today = DateTime.Now;
            Graph entry = (Graph)measurementTable.Execute(TableOperation.Retrieve<Graph>(ClientId, today.ToString("dd-MM-yyyy"))).Result;

            if (entry == null)
            {
                entry = new Graph(ClientId, today.ToString("dd-MM-yyyy"));
                entry.NumberofUsers = 1;
            }
            else
            {
                entry.NumberofUsers++;
            }

            measurementTable.Execute(TableOperation.InsertOrReplace(entry));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyCloudUniversityCommon;
using System.Text;
using System.Security.Cryptography;

namespace MyCloudUniversityWeb.Controllers
{
    public class UniversitiesController : Controller
    {
        private MyCloudUniversityContext db = new MyCloudUniversityContext();

        // GET: Universities
        public async Task<ActionResult> Index()
        {
            return View(await db.Universities.ToListAsync());
        }

        // GET: Universities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            University university = await db.Universities.FindAsync(id);
            if (university == null)
            {
                return HttpNotFound();
            }
            return View(university);
        }

        // GET: Universities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Universities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name,Abbr,DomainName")] University university)
        {
            if (ModelState.IsValid)
            {
                var existingUniversity = db.Universities
                    .Where(u => u.DomainName == university.DomainName)
                    .FirstOrDefault();
                if (existingUniversity == null)
                {
                    university.TableName = hashString(university.DomainName + university.Name);
                    db.Universities.Add(university);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                
            }

            return View(university);
        }

        // GET: Universities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            University university = await db.Universities.FindAsync(id);
            if (university == null)
            {
                return HttpNotFound();
            }
            return View(university);
        }

        // POST: Universities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Name,DomainName,AutoScale,ServicableUsers")] University university)
        {
            if (ModelState.IsValid)
            {
                University found = db.Universities
                    .Where(u => u.Name == university.Name && u.DomainName == university.DomainName)
                    .FirstOrDefault();
                if (found == null)
                {
                    return HttpNotFound(String.Format("University with name={0} and domain={1} does not exist", university.Name, university.DomainName));
                }
                found.DomainName = university.DomainName;
                found.Name = university.Name;
                found.AutoScale = university.AutoScale;
                found.ServicableUsers = university.ServicableUsers;
                db.Entry(found).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(university);
        }

        // GET: Universities/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            University university = await db.Universities.FindAsync(id);
            if (university == null)
            {
                return HttpNotFound();
            }
            return View(university);
        }

        // POST: Universities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            University university = await db.Universities.FindAsync(id);
            db.Universities.Remove(university);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private String hashString(String str){

            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            SHA256 sha = new SHA256CryptoServiceProvider();

            byte[] result = sha.ComputeHash(unicodeText);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}

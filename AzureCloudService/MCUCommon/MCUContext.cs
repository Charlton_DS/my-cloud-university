﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCUCommon
{
    public class MCUContext : DbContext
    {
        public MCUContext() : base ("name=MCUContext"){

        }

        public MCUContext(String connString)
            : base(connString)
        {

        }

        //Add Database Entities here
        public System.Data.Entity.DbSet<University> Universities { get; set; }

        public System.Data.Entity.DbSet<MCUCommon.AuthRequest> AuthRequests { get; set; }

        public System.Data.Entity.DbSet<MCUCommon.Course> Courses { get; set; }

    }
}

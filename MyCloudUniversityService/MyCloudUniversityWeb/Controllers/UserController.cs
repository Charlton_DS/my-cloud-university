﻿using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using MyCloudUniversityCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyCloudUniversityWeb.Controllers
{
    public class UserController : Controller
    {
        private MyCloudUniversityContext db = new MyCloudUniversityContext();
        public UserController()
        {
            initalize();
        }

        private void initalize()
        {
            var storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString"));
        }


        // GET: User
        public ActionResult Index()
        {
            return View();
        }
    }
}
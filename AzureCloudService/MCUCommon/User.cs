﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace MCUCommon
{
    public class User : TableEntity
    {
        public User(string university_id, string student_id)
        {
            this.PartitionKey = university_id;
            this.RowKey = student_id;
        }

        public User() { }

        public String Role { get; set; }

        public String Password { get; set; }

        public String Id { get; set; }

        public String FirstName { get; set; }

        public String MiddleName { get; set; }

        public String LastName { get; set; }

        public String Status { get; set; }

        public string Email { get; set; }

        public String PhoneNumber { get; set; }

        public String Address { get; set; }

        public String DOB { get; set; }

    }
}

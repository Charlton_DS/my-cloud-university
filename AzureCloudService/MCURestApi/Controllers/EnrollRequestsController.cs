﻿using MCUCommon;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace MCUWeb.Controllers
{
    /// <summary>
    /// Api for the management of user to course relationships
    /// </summary>
    public class EnrollRequestsController : ApiController
    {
        private MCUContext db = new MCUContext();
        private CloudTable enrollmentTable;
        private CloudTable coursesTable;
        private CloudQueue enrollmentQueue;

        public EnrollRequestsController()
        {
            IntializeStorage();
        }

        private void IntializeStorage()
        {

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            enrollmentTable = tableClient.GetTableReference("enrollment");
            enrollmentTable.CreateIfNotExists();

            coursesTable = tableClient.GetTableReference("universities");
            coursesTable.CreateIfNotExists();

            enrollmentQueue = queueClient.GetQueueReference("registration");
            enrollmentQueue.CreateIfNotExists();
        }

        /// <summary>
        /// Gets all courses user is enrolled in. 
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>All enrolled courses for user.</returns>
        [HttpGet]
        [Route("enroll/user/{id}")]
        [ResponseType(typeof(Course))]
        public IEnumerable<Course> GetCourses(string id)
        {
            TableQuery<Course> query = new TableQuery<Course>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, id));
            return enrollmentTable.ExecuteQuery(query);
        }

        /// <summary>
        /// Get a course enrollment for a user.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="cid">Course Id</param>
        /// <returns>User's enrollment</returns>
        [HttpGet]
        [Route("enroll/user/{id}/course/{cid}")]
        [ResponseType(typeof(Course))]
        public async Task<IHttpActionResult> GetCourse(string id , string cid)
        {
            TableResult result = await enrollmentTable.ExecuteAsync(TableOperation.Retrieve<Enroll>(id, cid));
            Enroll enroll = (Enroll)result.Result;
            if (enroll == null)
            {
                return NotFound();
            }

            return Ok(enroll);
        }

        /// <summary>
        /// Enrolls user in a course.
        /// </summary>
        /// <param name="request">Data needed for enrollment</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("enroll/register")]
        public async Task<IHttpActionResult> RegisterStudent(EnrollRequest request)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TableOperation retrieveOperation = TableOperation.Retrieve<Course>(request.UniversityId, request.CourseId);

            Course course = (Course)coursesTable.Execute(retrieveOperation).Result;

            retrieveOperation = TableOperation.Retrieve<User>(request.UniversityId, request.StudentId);

            User user = (User)coursesTable.Execute(retrieveOperation).Result;

            if(course != null && user != null)
            {
                
                if (user.Role == "Student" && course.NumStudents <= course.MaxStudents)
                {
                    course.NumStudents++;
                    await coursesTable.ExecuteAsync(TableOperation.Replace(course));
                    await enrollmentQueue.AddMessageAsync(new CloudQueueMessage(Stringify(request)));
                    return Ok();
                }
                else
                {
                    await enrollmentQueue.AddMessageAsync(new CloudQueueMessage(Stringify(request)));
                    return Ok();
                }
            }

            return NotFound();
        }

        /// <summary>
        /// Updates settings for a specified enrollment.
        /// </summary>
        /// <param name="request">Data to be updated</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("enroll/update")]
        public IHttpActionResult EditEnrollment(EnrollRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

			Enroll enrollment = (Enroll)enrollmentTable.Execute(TableOperation.Retrieve<Enroll>(request.StudentId, request.CourseId)).Result;
			if (enrollment != null)
			{
				if (request.Assignments != null)
				{
					enrollment.Assignments = request.Assignments;
					enrollment.NumberOfAssignments = (request.Assignments.Length / 8);
				}

				if (request.CourseWork != null)
				{
					enrollment.CourseWorkMarks = request.CourseWork;
					enrollment.NumberOfCourseWorkExams = (request.CourseWork.Length / 8);
				}

				if (request.Grade != null)
				{
					enrollment.Completed = true;
					enrollment.Grade = request.Grade;
				}
				return Ok();
			}
			return NotFound();
		}

        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        private String Stringify(EnrollRequest request)
        {
            String str = "" + request.StudentId + "," + request.CourseId; 
            return str;
        }

    }
}

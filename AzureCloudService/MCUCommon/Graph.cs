﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCUCommon
{
    public class Graph : TableEntity
    {
        public Graph(String UniversityId, String Date)
        {
            this.PartitionKey = UniversityId;
            this.RowKey = Date;
        }

        public Graph() { }

        public long NumberofUsers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace MyCloudUniversityCommon
{
    public class MyCloudUniversityContext : DbContext
    {
        public MyCloudUniversityContext() : base ("name=MyCloudUniversityContext"){

        }

        public MyCloudUniversityContext(String connString)
            : base(connString)
        {

        }

        //Add Database Entities here
        public System.Data.Entity.DbSet<University> Universities { get; set; }

        public System.Data.Entity.DbSet<MyCloudUniversityCommon.Course> Courses { get; set; }

    }
}

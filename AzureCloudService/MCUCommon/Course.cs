﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace MCUCommon
{
    public class Course : TableEntity
    {

        public Course(String universityId, String courseId)
        {
            this.PartitionKey = universityId;
            this.RowKey = courseId;
        }
        public Course() { }

        public String Id { get; set; }

        public String Name { get; set; }

        public String Programme { get; set; }

        public String Level { get; set; }

        public int NumStudents { get; set; }

        public int MaxStudents { get; set; }

        public String Semester { get; set; }
    }
}

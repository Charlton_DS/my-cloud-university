﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyCloudUniversityCommon;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage.Table;

namespace MyCloudUniversityWeb.Controllers
{
    public class CoursesController : Controller
    {
        private MyCloudUniversityContext db = new MyCloudUniversityContext();
        private CloudTable coursesTable;
        private void InitializeStorage()
        {
            var storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString"));            
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            coursesTable = tableClient.GetTableReference("courses");
            coursesTable.CreateIfNotExists();
        }

        public CoursesController()
        {
            InitializeStorage();
        }
        // GET: Courses
        //public async Task<ActionResult> Index()
        //{
        //    return View(await db.Courses.ToListAsync());
        //}

        //// GET: Courses/Details/5
        //public async Task<ActionResult> Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Course course = await db.Courses.FindAsync(id);
        //    if (course == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(course);
        //}

        // GET: Courses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Programme,Level,MaxStudents,PartitionKey,RowKey")] Course course)
        {
            if (ModelState.IsValid)
            {
                TableOperation retrieveOperation = TableOperation.Retrieve<Course>(course.PartionKey, course.RowKey);

                // Execute the retrieve operation.
                TableResult retrievedResult = coursesTable.Execute(retrieveOperation);
                if (retrievedResult.Result == null)
                {
                    TableOperation insertOperation = TableOperation.Insert(course);
                    coursesTable.Execute(insertOperation);
                    return RedirectToAction("Index");
                }
                
            }

            return View(course);
        }

        //// GET: Courses/Edit/5
        //public async Task<ActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Course course = await db.Courses.FindAsync(id);
        //    if (course == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(course);
        //}

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Programme,Level,MaxStudents,PartitionKey,RowKey")] Course course)
        {
            if (ModelState.IsValid)
            {

                TableOperation retrieveOperation = TableOperation.Retrieve<Course>(course.PartionKey, course.RowKey);

                // Execute the retrieve operation.
                TableResult retrievedResult = coursesTable.Execute(retrieveOperation);
                if (retrievedResult.Result != null)
                {
                    TableOperation replaceOperation = TableOperation.Replace(course);
                    coursesTable.Execute(replaceOperation);
                    return RedirectToAction("Index");
                }
            }
            return View(course);
        }

        //// GET: Courses/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Course course = await db.Courses.FindAsync(id);
        //    if (course == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(course);
        //}

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string university, string course)
        {
            //Course course = await db.Courses.FindAsync(id);
            //db.Courses.Remove(course);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

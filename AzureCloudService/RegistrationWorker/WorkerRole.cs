using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Queue;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MCUCommon;

namespace RegistrationWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        private CloudTable enrollmentTable;
        private CloudQueue enrollmentQueue;

        public override void Run()
        {
            Trace.TraceInformation("RegistrationWorker is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            enrollmentTable = tableClient.GetTableReference("enrollment");
            enrollmentTable.CreateIfNotExists();

            enrollmentQueue = queueClient.GetQueueReference("registration");
            enrollmentQueue.CreateIfNotExists();

            bool result = base.OnStart();

            Trace.TraceInformation("RegistrationWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("RegistrationWorker is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("RegistrationWorker has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            CloudQueueMessage message = null;
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");

                try
                {
                    message = enrollmentQueue.GetMessage();

                    if (message != null)
                    {
                        Trace.TraceInformation("queue");
                        EnrollRequest request = UnString(message.AsString);
                        Trace.TraceInformation("convert");
                        Enroll enroll = new Enroll(request.StudentId, request.CourseId);
                        enroll.Date = DateTime.Now;
                        enroll.Completed = false;
                        enroll.Semester = request.Semester;
                        Trace.TraceInformation("sent");
                        enrollmentTable.Execute(TableOperation.Insert(enroll));
                        enrollmentQueue.DeleteMessage(message);
                    }

                }
                catch (Exception e)
                {
                    Trace.TraceInformation(e.ToString());
                    if (message != null && message.DequeueCount > 5)
                    {
                        enrollmentQueue.DeleteMessage(message);
                        Trace.TraceError("Deleting poison queue item: '{0}'", message.AsString);
                    }
                }
                await Task.Delay(1000);
            }
        }

        private Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }
        private EnrollRequest UnString(String str)
        {
            String[] parts = str.Split(',');
            EnrollRequest request = new EnrollRequest();
            request.CourseId = parts[1];
            request.StudentId = parts[0];
            return request;
        }
    }
}

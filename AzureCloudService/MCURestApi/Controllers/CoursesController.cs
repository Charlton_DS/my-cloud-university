﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MCUCommon;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure;

namespace MCUWeb.Controllers
{
    /// <summary>
    /// Api for the management of courses for a client
    /// </summary>
    public class CoursesController : ApiController
    {
        private MCUContext db = new MCUContext();
        private CloudTable coursesTable;

        public CoursesController()
        {
            IntializeStoreage();
        }

        private void IntializeStoreage()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            coursesTable = tableClient.GetTableReference("universities");
            coursesTable.CreateIfNotExists();
        }


        // GET: api/Courses
        /// <summary>
        /// Gets all courses for a client.
        /// </summary>
        /// <param name="university">Cliend id</param>
        /// <returns>All courses for client.</returns>
        [HttpGet]
        [Route("universities/{university}/courses")]
        public IEnumerable<Course> GetCourses(String university)
        {
            TableQuery<Course> query = new TableQuery<Course>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, university));
            return coursesTable.ExecuteQuery(query); 
        }

        // GET: api/Courses/5
        /// <summary>
        /// Gets a course for a client.
        /// </summary>
        /// <param name="university">Client id</param>
        /// <param name="id">Course id</param>
        /// <returns>A course</returns>
        [HttpGet]
        [Route("universities/{university}/course/{id}")]
        [ResponseType(typeof(Course))]
        public async Task<IHttpActionResult> GetCourse(string university, string id)
        {
            TableResult result = await coursesTable.ExecuteAsync(TableOperation.Retrieve<Course>(university, id));
            Course course = (Course) result.Result;
            if (course == null)
            {
                return NotFound();
            }

            return Ok(course);
        }

        // PUT: api/Courses/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCourse(string id, Course course)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != course.Id)
            {
                return BadRequest();
            }

            db.Entry(course).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Courses
        /// <summary>
        /// Adds a course for a client.
        /// </summary>
        /// <param name="course">Data on course to be added</param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [HttpPost]
        [Route("universities/course")]
        public async Task<IHttpActionResult> addCourse(Course course)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await coursesTable.ExecuteAsync(TableOperation.Insert(course));

            return Ok();
        }

        /// <summary>
        /// Adds a batch of courses for a client.
        /// </summary>
        /// <param name="courses">An array of courses to be added</param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [HttpPost]
        [Route("universities/courses")]
        public async Task<IHttpActionResult> addCourses(Course [] courses)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            TableBatchOperation batch = new TableBatchOperation();

            foreach (Course course in courses){
                batch.Insert(course);
            }
            await coursesTable.ExecuteBatchAsync(batch);

            return Ok();
        }

        // DELETE: api/Courses/5
        /// <summary>
        /// Deletes a course from a client
        /// </summary>
        /// <param name="university">Client Id</param>
        /// <param name="id">Course Id</param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [HttpDelete]
        [Route("universities/{university}/course/{id}")]
        public async Task<IHttpActionResult> DeleteCourse(string university, string id)
        {
            TableResult result = await coursesTable.ExecuteAsync(TableOperation.Retrieve<Course>(university, id));
            Course course = (Course) result.Result;
            if (course == null)
            {
                return NotFound();
            }

            await coursesTable.ExecuteAsync(TableOperation.Delete(course));

            return Ok(course);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CourseExists(string id)
        {
            return db.Courses.Count(e => e.Id == id) > 0;
        }
    }
}
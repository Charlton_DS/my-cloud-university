﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MCUCommon;
using System.Text;
using System.Security.Cryptography;
using Microsoft.WindowsAzure;

namespace MCUWeb.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class UniversitiesController : ApiController
    {
        private MCUContext db;


        public UniversitiesController()
        {
            var dbConnString = CloudConfigurationManager.GetSetting("MCUDbConnectionString");
            db = new MCUContext(dbConnString);
        }
        // GET: api/Universities
        [ApiExplorerSettings(IgnoreApi = true)]
        public IQueryable<University> GetUniversities()
        {
            return db.Universities;
        }

        // GET: api/Universities/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(University))]
        public async Task<IHttpActionResult> GetUniversity(int id)
        {
            University university = await db.Universities.FindAsync(id);
            if (university == null)
            {
                return NotFound();
            }

            return Ok(university);
        }


        // PUT: api/Universities/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUniversity(int id, University university)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != university.Id)
            {
                return BadRequest();
            }

            db.Entry(university).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UniversityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Universities
        /// <summary>
        /// Creates a new Client
        /// </summary>
        /// <param name="university">Client Credentials</param>
        /// <returns></returns>
        [HttpPost]
        [Route("universities/create")]
        [ResponseType(typeof(University))]
        public async Task<IHttpActionResult> CreateUniversity(University university)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            University temp = await db.Universities
                    .Where(u => u.DomainName == university.DomainName)
                    .FirstOrDefaultAsync();
            if (temp == null)
            {
                university.TableName = hashString(university.DomainName + university.Abbr);
                db.Universities.Add(university);
                await db.SaveChangesAsync();
                return Ok(university.TableName);
            }
            return BadRequest();
        }

        /// <summary>
        /// Updates client credentials.
        /// </summary>
        /// <param name="university">New client credentials</param>
        /// <returns></returns>
        [HttpPost]
        [Route("universities/edit")]
        public async Task<IHttpActionResult> EditUniversity(University university)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            University temp = await db.Universities
                    .Where(u => u.TableName == university.TableName)
                    .FirstOrDefaultAsync();
            university.Id = temp.Id;
            university.TableName = hashString(university.DomainName + university.Abbr);
            db.Entry(university).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Ok();
        }


        // DELETE: api/Universities/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(University))]
        public async Task<IHttpActionResult> DeleteUniversity(int id)
        {
            University university = await db.Universities.FindAsync(id);
            if (university == null)
            {
                return NotFound();
            }

            db.Universities.Remove(university);
            await db.SaveChangesAsync();

            return Ok(university);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private bool UniversityExists(int id)
        {
            return db.Universities.Count(e => e.Id == id) > 0;
        }

        private String hashString(String str)
        {

            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            SHA256 sha = new SHA256CryptoServiceProvider();

            byte[] result = sha.ComputeHash(unicodeText);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
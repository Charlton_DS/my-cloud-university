﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCloudUniversityCommon
{
    public class University
    {
        public int Id { get; set; }

        [StringLength(100)]
        public String Name { get; set; }

        [StringLength(10)]
        public String Abbr { get; set; }

        [StringLength(50)]
        public String DomainName { get; set; }

        public Boolean AutoScale { get; set; }

        public long ServicableUsers { get; set; }

        public double CurrentUsageCost { get; set; }

        public String TableName { get; set; }
    }
}

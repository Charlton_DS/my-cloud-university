﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCloudUniversityCommon
{
    class User : TableEntity
    {
        public User(String university, String id)
        {
            this.PartitionKey = university;
            this.RowKey = id;
        }

        public User()
        {

        }

        public String FirstName { get; set; }

        public String MiddleName { get; set; }

        public String LastName { get; set; }

        public String DateOfBirth { get; set; }

        public String Address { get; set; }

        public String UserId { get; set; }

        public String Type { get; set; }
    }
}

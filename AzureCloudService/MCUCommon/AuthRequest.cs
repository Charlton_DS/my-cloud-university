﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCUCommon
{
    public class AuthRequest
    {
        public AuthRequest(String Id, String Password)
        {
            this.Id = Id;
            this.Password = Password;
        }

        public AuthRequest() { }

        public String Id { get; set; }

        public String Password { get; set; }
    }
}

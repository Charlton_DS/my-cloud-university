﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MCUCommon;
using System.Text;
using System.Security.Cryptography;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage;

namespace MCURestApi.Controllers
{
    /// <summary>
    /// Api for the management of client service settings
    /// </summary>
    public class SelfServiceController : ApiController
    {
        private MCUContext db;
        private CloudTable measurementTable;

        /// <summary>
        /// Constructor for class
        /// </summary>
        public SelfServiceController()
        {
            var dbConnString = CloudConfigurationManager.GetSetting("MCUDbConnectionString");
            db = new MCUContext(dbConnString);


            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            measurementTable = tableClient.GetTableReference("measurement");
            measurementTable.CreateIfNotExists();
        }

        // GET: api/SelfService
        /// <summary>
        /// Gets information on a My Cloud University Client.
        /// </summary>
        /// <param name="id">Client Id</param>
        /// <returns>Client service information</returns>
        [ResponseType(typeof(SelfService))]
        [HttpGet]
        [Route("client/{id}")]
        public async Task<IHttpActionResult> GetCLient(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            
            University university = await db.Universities
                .Where(u => u.TableName == id)
                .FirstOrDefaultAsync();
            
            if (university != null)
            {
                return Ok(new SelfService(university));
            }

            return NotFound();
        }

        /// <summary>
        /// Gets number of a active student users in the given time period.
        /// </summary>
        /// <param name="id">Client Id</param>
        /// <param name="start">Start date</param>
        /// <param name="end">End date</param>
        /// <returns></returns>
        [ResponseType(typeof(Graph[]))]
        [HttpGet]
        [Route("client/{id}/startDate/{start}/endDate/{end}")]
        public async Task<IHttpActionResult> GetEntries(string id, string start, string end )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Graph[] entries;
            DateTime startDate;
            DateTime endDate;
            try
            {
                startDate = DateTime.Parse(start);
                endDate = DateTime.Parse(end);
            }

            catch(Exception e){
                return BadRequest();
            }

            TimeSpan span = endDate.Subtract(startDate);
            entries = new Graph[(int)span.TotalDays];
            
            if (DateTime.Compare(startDate, endDate) <= 0 && DateTime.Compare(endDate, DateTime.Today) <= 0)
            {
                TableBatchOperation batch = new TableBatchOperation();
                int i = 0;
                while (DateTime.Compare(startDate, endDate) <= 0)
                {
                    TableResult result = await measurementTable.ExecuteAsync(TableOperation.Retrieve<Graph>(id, startDate.ToString("dd-MM-yyyy")));
                    Graph graph = (Graph)result.Result;
                    if (graph != null)
                    {
                        entries[i] = graph;
                        i++;
                    }
                    startDate = startDate.AddDays(1);
                }                

                if (entries.Length == 0)
                {
                    return NotFound();
                }
                               
                return Ok(entries);
            }

            return BadRequest("Date Mismatch");
        }

        // GET: api/SelfService/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/SelfService
        /// <summary>
        /// Update clients measurement settings.
        /// </summary>
        /// <param name="service">Configuration Options</param>
        /// <returns></returns>
        [HttpPost]
        [Route("client")]
        public async Task<IHttpActionResult> EditClient (SelfService service)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            University university = await db.Universities
                    .Where(u => u.TableName == service.Id)
                    .FirstOrDefaultAsync();
            if (university == null)
            {
                
                if(service.AutoScale.HasValue){
                    university.AutoScale = (bool)service.AutoScale;
                }

                if(service.MaxUsers.HasValue){
                    university.ServicableUsers = (long)service.MaxUsers;
                }

                if(service.Tier.HasValue){


                }

                db.Entry(university).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return Ok();
            }

            return NotFound();


        }

        [ApiExplorerSettings(IgnoreApi = true)]
        // PUT: api/SelfService/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        // DELETE: api/SelfService/5
        public void Delete(int id)
        {
        }
    }
}

﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCUCommon
{
    public class Semester : TableEntity
    {
        public Semester(String universityId, String semesterName)
        {
            this.PartitionKey = universityId;
            this.RowKey = semesterName;
        }

        public Semester() { }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}

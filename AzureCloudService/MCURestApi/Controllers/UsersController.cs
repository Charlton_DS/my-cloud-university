﻿using MCUCommon;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace MCURestApi.Controllers
{
    /// <summary>
    /// Api for the management of users for a client
    /// </summary>
    public class UsersController : ApiController
    {
        private CloudTable userTable;
        public UsersController(){

            IntializeStorage();
        }

        private void IntializeStorage(){
            
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            userTable = tableClient.GetTableReference("universities");
            userTable.CreateIfNotExists();
        }
        // GET: api/Students
        /// <summary>
        /// Gets all the users for a client.
        /// </summary>
        /// <param name="university"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("universities/{university}/students")]
        public IEnumerable<User> GetStudents(string university)
        {
            TableQuery<User> query = new TableQuery<User>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, university));
            return userTable.ExecuteQuery(query); 
        }

        // GET: api/Students/5
        /// <summary>
        /// Gets a user for a client.
        /// </summary>
        /// <param name="university">Client id</param>
        /// <param name="id">User id</param>
        /// <returns>User</returns>
        [HttpGet]
        [Route("universities/{university}/user/{id}")]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetStudent(string university, string id)
        {
            TableResult result = await userTable.ExecuteAsync(TableOperation.Retrieve<User>(university, id));
            User student = (User) result.Result;
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        // POST: api/Students
        /// <summary>
        /// Adds a user to a client
        /// </summary>
        /// <param name="student">Data for user</param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [HttpPost]
        [Route("universities/student")]
        public async Task<IHttpActionResult> addStudent(User student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await userTable.ExecuteAsync(TableOperation.Insert(student));

            return Ok();
        }


        /// <summary>
        /// Edits data of a user for a client.
        /// </summary>
        /// <param name="student">Updated data</param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("universities/student")]
        public async Task<IHttpActionResult> editStudent(User student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await userTable.ExecuteAsync(TableOperation.Merge(student));

            return Ok();
        }

        // POST: api/Students/5
        /// <summary>
        /// Adds a batch of users for a client
        /// </summary>
        /// <param name="students">Array of users</param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [HttpPost]
        [Route("universities/students")]
        public async Task<IHttpActionResult> addUsers(User[] students)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            TableBatchOperation batch = new TableBatchOperation();

            foreach (User student in students)
            {
                batch.Insert(student);                     
            }
            await userTable.ExecuteBatchAsync(batch);

            return Ok();
        }

        // DELETE: api/Students/5
        /// <summary>
        /// Removes a user from a client
        /// </summary>
        /// <param name="university">Client Id</param>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [HttpDelete]
        [Route("universities/{university}/student/{id}")]
        public async Task<IHttpActionResult> DeleteStudent(string university, string id)
        {
            TableResult result = await userTable.ExecuteAsync(TableOperation.Retrieve<Course>(university, id));
            User student = (User)result.Result;
            if (student == null)
            {
                return NotFound();
            }

            await userTable.ExecuteAsync(TableOperation.Delete(student));

            return Ok(student);
        }
    }
}

﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCUCommon
{
    public class Enroll : TableEntity
    {
        public Enroll(String StudentId, String CourseId)
        {
            this.PartitionKey = StudentId;
            this.RowKey = CourseId;
        }

        public Enroll() { }

        public DateTime Date { get; set; }

        public Boolean Completed { get; set; }

        public String Grade { get; set; }

        public String Semester { get; set; }

        public String Assignments { get; set; }

        public String CourseWorkMarks { get; set; }

        public int NumberOfAssignments { get; set; }

        public int NumberOfCourseWorkExams { get; set; }
    }
}

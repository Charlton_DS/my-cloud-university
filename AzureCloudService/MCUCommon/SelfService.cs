﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCUCommon
{
    public class SelfService
    {
        public SelfService(University university)
        {
            Id = university.TableName;
            MaxUsers = university.ServicableUsers;
            CurrentUsers = university.CurrentUsers;
            AutoScale = university.AutoScale;
            CurrentBilling = university.CurrentUsageCost;
        }

        public SelfService() { }

        public string Id { get; set; }

        public long? MaxUsers {get; set;}

        public int? Tier { get; set; }

        public long? CurrentUsers { get; set; }

        public bool? AutoScale { get; set; }

        public double CurrentBilling { get; set; }
    }
}

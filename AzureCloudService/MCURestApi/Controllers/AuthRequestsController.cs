﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MCUCommon;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.ServiceBus;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace MCUWeb.Controllers
{
    /// <summary>
    /// Api for the authentication of users
    /// </summary>
    public class AuthRequestsController : ApiController
    {
        private MCUContext db;
        private CloudTable universityTable;
        private CloudQueue measurementQueue;

        public AuthRequestsController()
        {
            var dbConnString = CloudConfigurationManager.GetSetting("MCUDbConnectionString");
            db = new MCUContext(dbConnString);

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            universityTable = tableClient.GetTableReference("universities");

            CloudQueueClient queueCluient = storageAccount.CreateCloudQueueClient();
            measurementQueue = queueCluient.GetQueueReference("measurment");
            measurementQueue.CreateIfNotExists();
            
            
        }
        
        
        // GET: api/AuthRequests/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(AuthRequest))]
        public async Task<IHttpActionResult> GetAuthRequest(string id)
        {
            AuthRequest authRequest = await db.AuthRequests.FindAsync(id);
            if (authRequest == null)
            {
                return NotFound();
            }

            return Ok(authRequest);
        }

        // PUT: api/AuthRequests/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAuthRequest(string id, AuthRequest authRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != authRequest.Id)
            {
                return BadRequest();
            }

            db.Entry(authRequest).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuthRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/AuthRequests
        /// <summary>
        /// Authenticates user.
        /// </summary>
        /// <param name="authRequest">User credentials</param>
        /// <returns>User object</returns>
        [HttpPost]
        [Route("auth/login")]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostAuthRequest(AuthRequest authRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
          
            String[] parts = authRequest.Id.Split('@');
            String domain = parts[1];
            University university = await db.Universities
                .Where(u => u.DomainName == domain)
                .FirstOrDefaultAsync();
            if (university != null)
            {
                User student = (User)universityTable.Execute(TableOperation.Retrieve<User>(university.TableName, parts[0])).Result;
               
                if (student == null)
                {
                    return NotFound();
                }

                if (student.Password == authRequest.Password)
                {
                    if (student.Role == "Student")
                    {
                        measurementQueue.AddMessage(new CloudQueueMessage("login," + university.TableName));
                    }
                    return Ok(student);
                }
            }

            return NotFound();

        }

        /// <summary>
        /// Deauthenticates user.
        /// </summary>
        /// <param name="authRequest">User credentials</param>
        /// <returns></returns>
        [HttpPost]
        [Route("auth/logout")]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> logout(AuthRequest authRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            String[] parts = authRequest.Id.Split('@');
            String domain = parts[1];
            University university = await db.Universities
                .Where(u => u.DomainName == domain)
                .FirstOrDefaultAsync();

            if (university != null)
            {
                User student = (User)universityTable.Execute(TableOperation.Retrieve<User>(university.TableName, parts[0])).Result;
                if (student.Password == authRequest.Password)
                {
                    if (student.Role == "Student")
                    {
                        measurementQueue.AddMessage(new CloudQueueMessage("logout," + university.TableName));
                    }
                    return Ok(student);
                }
            }

            return NotFound();

        }

        // DELETE: api/AuthRequests/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(AuthRequest))]
        public async Task<IHttpActionResult> DeleteAuthRequest(string id)
        {
            AuthRequest authRequest = await db.AuthRequests.FindAsync(id);
            if (authRequest == null)
            {
                return NotFound();
            }

            db.AuthRequests.Remove(authRequest);
            await db.SaveChangesAsync();

            return Ok(authRequest);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AuthRequestExists(string id)
        {
            return db.AuthRequests.Count(e => e.Id == id) > 0;
        }

        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
    }
}